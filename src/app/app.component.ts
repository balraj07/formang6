import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../node_modules/@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'reactiveForm';

  form: FormGroup;
  constructor(private fb: FormBuilder){
    this.form = this.fb.group({
      lname: ['',Validators.pattern('[A-Z ]*')],
      fname: ['',Validators.pattern('[A-Z ]*')],
      email: ['',Validators.pattern('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/')],
      contNO: ['',Validators.pattern('[0-9 ]*')],
      pass: '',
      cnfrm_pass: '',
      emp_id: ['',Validators.maxLength(4)]
    });
  }

  onSubmit(){
    console.log(this.form.value);
  }
}
